#!/bin/bash

# This script replaces all text occurencies of OLD_TEXT with NEW_TEXT
# in all the files passed as arguments

# Check arguments
if (( $# < 3 )); then
    echo "Wrong usage!"
    echo "replace-text.sh OLD_TEXT NEW_TEXT file1 file2 file3 ... fileN"
    exit 1
fi
old=$1
new=$2

# Go with subtitutions!
for file in ${@: 3}
do
  if [ -f $file ]; then
   sed -i "s/$old/$new/g" "$file"
   # echo "ciao"
  fi
done
